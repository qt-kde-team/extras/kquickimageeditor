kquickimageeditor (0.5.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.7.2, no changes required.

 -- Pino Toscano <pino@debian.org>  Mon, 03 Mar 2025 06:52:59 +0100

kquickimageeditor (0.5.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (0.5.0).
  * Added myself to the uploaders.

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 23 Jan 2025 15:27:03 +0100

kquickimageeditor (0.4.0-3) unstable; urgency=medium

  * Team upload.

  [ Aurélien COUDERC ]
  * Add/update Carl Schwan’s key in upstream keyring.
  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 13 Dec 2024 10:28:01 +0100

kquickimageeditor (0.4.0-2) experimental; urgency=medium

  [ Sandro Knauß ]
  * Use dh_qmldeps to detect QML dependencies.
  * Temporarily add qt6-declarative-private-dev to workaround #1087385.
  * Update list of needed QML modules.
  * Examples shipped in dev package depends on QML modules add those as
    suggests.

 -- Sandro Knauß <hefee@debian.org>  Mon, 25 Nov 2024 17:40:48 +0100

kquickimageeditor (0.4.0-1) experimental; urgency=medium

  [ Vincent Pinon ]
  * New upstream release (0.4.0).
  * Update build-deps and deps with the info from cmake.
  * Bump Standards-Version to 4.7.0 (no changes needed).
  * Move build to Qt6
    - Rename qml- package to qml6-.

  [ Sandro Knauß ]
  * Use dh-sequence instead of --with.
  * Mark packages as Multi-Arch: same.
  * Use pkgkde-getqmldepends.
  * d/copyright: Add Upstream-Name and Source.

 -- Sandro Knauß <hefee@debian.org>  Thu, 03 Oct 2024 00:16:33 +0200

kquickimageeditor (0.3.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.6.2, no changes needed.
  * Update the upstream GPG signing key.
  * Update the build dependencies according to the upstream build system:
    - bump extra-cmake-modules to 5.91
    - bump Qt 5 packages to 5.15.0
  * Small copyright changes.

 -- Pino Toscano <pino@debian.org>  Fri, 03 Nov 2023 07:16:54 +0100

kquickimageeditor (0.2.0-4) unstable; urgency=medium

  * Team upload.
  * Install the qmake development stuff in kquickimageeditor-dev, rather than
    qml-module-org-kde-kquickimageeditor
    - add proper breaks/replaces
  * Drop the qml-module-qtquick2 dependency in kquickimageeditor-dev, as it is
    a dependency of qml-module-org-kde-kquickimageeditor already.
  * Use dh_installexamples to install the examples in kquickimageeditor-dev,
    rather than dh_install.
  * Remove the unused ${shlibs:Depends} substvar from kquickimageeditor-dev.

 -- Pino Toscano <pino@debian.org>  Sun, 27 Feb 2022 08:11:58 +0100

kquickimageeditor (0.2.0-3) unstable; urgency=medium

  * Set correct Debian maintainer information.

 -- Sandro Knauß <hefee@debian.org>  Tue, 22 Feb 2022 12:23:14 +0100

kquickimageeditor (0.2.0-2) unstable; urgency=medium

  * Set upstream metadata fields: Bug-Database, Bug-Submit.

 -- Sandro Knauß <hefee@debian.org>  Mon, 21 Feb 2022 17:28:59 +0100

kquickimageeditor (0.2.0-1) unstable; urgency=medium

  [ Norbert Preining ]
  * First upload to Debian (Closes: #989528)

  [ Sandro Knauß ]
  * Finish last bits to finally upload.

 -- Sandro Knauß <hefee@debian.org>  Sat, 19 Feb 2022 15:27:58 +0100
